import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from "./plugins/vuetify";
import "animate.css";
import axios from 'axios';
import "./sass/overrides.scss";
import { baseUrlAxios } from './utils/globalVariables';

axios.defaults.baseURL = baseUrlAxios;
Vue.config.productionTip = false;

new Vue({
    vuetify,
    store,
    render: h => h(App)
}).$mount('#assistant-chatbot')
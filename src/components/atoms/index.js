export { default as AtomAvatar } from './AtomAvatar.vue';
export { default as AtomTyping } from './AtomTyping.vue';
export { default as AtomText } from './AtomText.vue';
export * from './icons'
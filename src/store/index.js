import Vue from 'vue'
import Vuex from 'vuex'
import { v4 as uuidv4 } from 'uuid';
import { requests, pusherId, pusherConfig, agentDerivationFunctionality } from "../utils/globalVariables";
import { getAlertData } from "../utils/alertData";
import { EventBus } from "../EventBus";
import Pusher from "pusher-js";

Vue.use(Vuex)

const getTime = (creationHour) => {
    const currentHour = new Date().getTime();
    const timeDifference = currentHour - creationHour;
    const difInSeconds = timeDifference / 1000;
    if (difInSeconds < 30) {
        return "Ahora";
    } else if (difInSeconds >= 30 & difInSeconds < 60) {
        return "Hace un momento";
    }
    else if (difInSeconds >= 60 & difInSeconds < 120) {
        return "Hace 1 minuto";
    } else if (difInSeconds >= 120 & difInSeconds < 3600) {
        const minute = Math.floor(difInSeconds / 60)
        return `Hace ${minute} minutos`;
    }
    else if (difInSeconds >= 3600) {
        return "Hace más de una hora";
    }
}

const store = new Vuex.Store({
    state: {
        basic_auth: {
            auth: {
                username: "api@innovit.cl",
                password: "christusqa2020"
            }
        },
        lastMessage: {
            text: "",
            time: "",
            status: false,
            creationHour: new Date(),
            alertType: "",
        },
        messagesChat: [
            {
                id: uuidv4(),
                text: "",
                type: "bot",
                options: {},
                alert: {}
            }
        ],
        token: "",
        lastRequestChat: {
            type: "", // text / option / token / agent
            intent: "", // save only if request is option
            text: "" // save only if request is text or agent
        },
        channel: "",
        channelSuscription: false,
        irisInteraction: false,
        retryIrisInteraction: 0,
        agent: { id: null, firstName: null, lastName: null },
        agentTransfer: false,
        statusChatbot: false,   // variable for chatbot vidibility
        LastIntent: {
            name: "none",
            repetition: 0
        },
        activePage: "chat",     // initial page
        hasNotification: {
            chat: false
        },
        chatOpenFirstTime: false,
        agentDerivationStatus: false,
        derivationPending: {
            time: '',
            creationHour: new Date()
        },
        whoToDerive: "" //variable que determinará a que api solicitar agente
    },
    mutations: {
        saveAgentTransfer(state, agentTransfer) {
            state.agentTransfer = agentTransfer
        },
        saveAgentData(state, agentData) {
            state.agent = agentData
        },
        saveIrisInteraction(state, status) {
            state.irisInteraction = status
        },
        saveChannelSuscription(state, status) {
            state.channelSuscription = status
        },
        saveAlertDataToAgent(state, payload) {
            state.messagesChat[state.messagesChat.length - 1].alert = payload;
        },
        resetAgentData(state) {
            state.agent.id = null;
            state.agent.firstName = null;
            state.agent.lastName = null;
        },
        // save data of lastMessage from watson
        saveLastMessage(state, type) {
            switch (type) {
                case "chat":
                    state.lastMessage.text = state.messagesChat[state.messagesChat.length - 1].text.slice(0, 30) + '...';
                    state.lastMessage.time = "Ahora";
                    state.lastMessage.status = true;
                    state.lastMessage.creationHour = new Date();
                    EventBus.$emit('saveLastMessageStore')
                    break;
                default:
                    break;
            }
        },
        // verify the creationHour from lastMessage and the current hour to set the status
        setTimeLastMessage(state) {
            if (state.lastMessage.text !== '') {
                const previuosTime = state.lastMessage.time;
                state.lastMessage.time = getTime(state.lastMessage.creationHour.getTime())
                if (previuosTime !== state.lastMessage.time) {
                    EventBus.$emit('updateLastMessageTimeStore');
                }
            }
        },
        // add a message to array messages
        saveMessage(state, payload) {
            payload.id = uuidv4();
            state.messagesChat.push(payload);
        },
        replaceLastMessage(state, payload) {
            payload.id = uuidv4();
            state.messagesChat[state.messagesChat.length - 1] = payload;
        },
        deleteLastMessage(state) {
            state.messagesChat.pop();
        },
        saveAgentMessage(state, payload) {
            payload.id = uuidv4();
            state.messagesChat.splice(state.messagesChat.length - 1, 0, payload);
        },
        saveToken(state, payload) {
            state.token = payload.token;
            const pusher = new Pusher(pusherId, pusherConfig);
            const channel = pusher.subscribe(`chat-bot-${payload.token}`);
            state.channel = channel;
        },
        updateLastMessageOfArray(state, payload) {
            if (payload.text !== undefined) {
                state.messagesChat[state.messagesChat.length - 1].text = payload.text;
            }

            if (payload.options !== undefined) {
                state.messagesChat[state.messagesChat.length - 1].options = payload.options
            }
            if (payload.alert_type !== undefined) {
                EventBus.$emit("disableFooterButton");
                let alert = {};
                if (payload.alert_type !== '') {
                    alert = getAlertData(payload.alert_type);
                    alert.show = true;
                    alert.closeEvent = 'closeChatAlert'
                }
                state.messagesChat[state.messagesChat.length - 1].alert = alert;
            }
        },
        updateLastRequestChat(state, payload) {
            let type = "";
            let intent = "";
            let text = "";

            if (payload.type !== undefined) {
                type = payload.type
            }

            if (payload.intent !== undefined) {
                intent = payload.intent
            }

            if (payload.text !== undefined) {
                text = payload.text
            }

            state.lastRequestChat.type = type;
            state.lastRequestChat.intent = intent;
            state.lastRequestChat.text = text;
        },
        changeStatusChatbot(state) {
            state.statusChatbot = !state.statusChatbot;
        },
        updateLastIntent(state, payload) {
            state.LastIntent.repetition++;
        },
        resetLastIntent(state) {
            state.LastIntent.repetition = 0;
        },
        updateActivePage(state, page) {
            state.activePage = page;
        },
        updateHasNotification(state, payload) {
            state.hasNotification.chat = payload.show;
        },
        updateAgentDerivationStatus(state, payload) {
            state.agentDerivationStatus = payload;
            EventBus.$emit('updateAgentDerivationStatus', payload);
        },
        updateIrisInteractionStatus(state) {
            EventBus.$emit('updateIrisInteractionStatus');
        },
        saveDerivationPending(state) {
            state.derivationPending.time = 'Ahora',
                state.derivationPending.creationHour = new Date()
        },
        setDerivationPendingTime(state) {
            const previuosTime = state.derivationPending.time;
            state.derivationPending.time = getTime(state.derivationPending.creationHour.getTime())
            if (previuosTime !== state.derivationPending.time) {
                EventBus.$emit('updateAgentDerivationStatus', true);
            }
        },
        setWhoToDerive(state, payload) {
            state.whoToDerive = payload;
        }
    },
    actions: {
        async getToken({ commit, state }) {
            try {
                const { data } = await requests.getToken({ method: "session" }, state.basic_auth);
                commit("saveToken", { token: data.token });
                commit("updateLastMessageOfArray", { text: data.message, alert_type: '' });
                commit("saveLastMessage", 'chat');
                checkChatNotifications({ commit, state });
            } catch (error) {
                let newMessageData = {
                    text: '¡Parece que algo no anda bien!...',
                    alert_type: 'server_error'
                }
                commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
                commit("updateLastRequestChat", { type: 'token' });
                checkChatNotifications({ commit, state });
            }
        },
        async sendMessageToSimone({ commit, state }, payload) {
            const params = { header: state.token, data: payload };
            let newMessageData = "";
            try {
                const { data } = await requests.sendMessageToSimone(params);
                // Lógica para despliegue de alerta
                if (data.alert_type !== '') {
                    newMessageData = {
                        text: '¡Parece que algo no anda bien!...',
                        alert_type: data.alert_type
                    }
                }
                // commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
            } catch (error) {
                console.log(error);
                newMessageData = {
                    text: '¡Parece que algo no anda bien...',
                    alert_type: 'message_server_error',
                }
                /* Agregar mensaje vacio si no hay uno anteriormente */
                if (state.messagesChat[state.messagesChat.length - 1].text !== "") {
                    commit("saveMessage", { text: "", type: "alert" });
                }

                commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
                commit("updateLastRequestChat", { type: 'agent', text: payload.message });
            }
        },
        async sendMessageToGenesys({ commit, state }, payload) {
            const params = { header: state.token, data: payload };
            let newMessageData = "";
            try {
                const { data } = await requests.sendMessageToGenesys(params);
                // Lógica para despliegue de alerta
                if (data.alert_type !== '') {
                    newMessageData = {
                        text: '¡Parece que algo no anda bien!...',
                        alert_type: data.alert_type
                    }
                }
                // commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
            } catch (error) {
                console.log(error);
                newMessageData = {
                    text: '¡Parece que algo no anda bien...',
                    alert_type: 'message_server_error',
                }
                /* Agregar mensaje vacio si no hay uno anteriormente */
                if (state.messagesChat[state.messagesChat.length - 1].text !== "") {
                    commit("saveMessage", { text: "", type: "alert" });
                }

                commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
                commit("updateLastRequestChat", { type: 'agent', text: payload.message });
            }
        },
        async sendMessage({ commit, state }, message) {
            const params = { header: state.token, data: message };
            let newMessageData = "";
            try {
                const { data } = await requests.sendMessage(params, state.basic_auth);
                if (data){
                    const incomingIntent = data.data.response[0].intent;
                    // Logica para mensaje directo
                    if (data.data.response_type === 'text') {
                        switch (incomingIntent) {
                            case "TRANSFERENCIA":
                                EventBus.$emit("disableFooterButton");
                                let messageOptions = {
                                    type: 'confirmation'
                                }
                                newMessageData = {
                                    text: '¿Te gustaría comunicarte con uno de nuestros ejecutivos?',
                                    options: messageOptions,
                                    alert_type: ''
                                };
                                commit('resetLastIntent');
                                break;
    
                            case "none":
                                commit("updateLastIntent");
                                if (state.LastIntent.repetition === 3 && agentDerivationFunctionality) {
                                    EventBus.$emit("disableFooterButton");
                                    let messageOptions = {
                                        type: 'confirmation_agent_derivation'
                                    }
                                    newMessageData = {
                                        text: '¡Lo siento! No entiendo lo que necesitas. <b>¿Te gustaría continuar tu atención con uno de nuestros ejecutivos?</b>',
                                        options: messageOptions,
                                        alert_type: ''
                                    };
                                    commit('resetLastIntent');
                                } else {
                                    newMessageData = { text: 'No entiendo lo que necesitas, por favor revisa tu pregunta e inténtalo nuevamente.', alert_type: '' };
                                }
                                break;
    
                            default:
                                newMessageData = { text: data.data.response[0].text, alert_type: '' };
                                commit('resetLastIntent');
                                break;
                        }
    
                    }

                    if (data.data.response_type === 'suggestion') {
                        let messageOptions = {
                            type: 'multipleChoice',
                            options: []
                        }
                        data.data.response.forEach(option => {
                            messageOptions.options.push({ text: option.option, intent: option.intent });
                        });
                        newMessageData = {
                            text: data.data.title,
                            options: messageOptions,
                            alert_type: ''
                        };
                    }
                    // Lógica para despliegue de alerta
                    if (data.alert_type !== '') {
                        newMessageData = {
                            text: '¡Parece que algo no anda bien!...',
                            alert_type: data.alert_type
                        }
                    }
                    if (data.data.response.length >= 2) {
                        commit("deleteLastMessage");
                        for (let i of data.data.response) {
                            newMessageData = {
                                text: i.text,
                                alert_type: ''
                            };
                            commit("saveMessage", { text: i.text, type: "bot", options: {}, alert: {} });
                            commit("saveLastMessage", 'chat');
                            checkChatNotifications({ commit, state });
                        }
                    } else {
                        commit("updateLastMessageOfArray", newMessageData);
                        commit("saveLastMessage", 'chat');
                        checkChatNotifications({ commit, state });
                    }
                }else{
                    let data = {
                        text: "Un momento por favor.",
                        alert_type: '',
                    }
                    commit("updateLastMessageOfArray", data);
                }
            } catch (error) {
                console.log(error);
                newMessageData = {
                    text: '¡Parece que algo no anda bien!...',
                    alert_type: 'message_server_error'
                }
                commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
                commit("updateLastRequestChat", { type: 'text', text: message });
            }

        },
        async sendOption({ commit, state }, { intent, text }) {
            const params = { header: state.token, data: { intent, text } };
            let newMessageData = "";
            try {
                const { data } = await requests.sendOption(params);
                const incomingIntent = data.data.response[0].intent;
                // Logica para mensaje directo
                switch (incomingIntent) {
                    case "none":
                        commit("updateLastIntent");
                        if (state.LastIntent.repetition === 3) {
                            let messageOptions = {
                                type: 'confirmation_agent_derivation'
                            }
                            newMessageData = {
                                text: '¡Lo siento! No entiendo lo que necesitas. <b>¿Te gustaría continuar tu atención con uno de nuestros ejecutivos?</b>',
                                options: messageOptions,
                                alert_type: ''
                            };
                            commit('resetLastIntent');
                        } else {
                            newMessageData = { text: 'No entiendo lo que necesitas, por favor revisa tu pregunta e inténtalo nuevamente.', alert_type: '' };
                        }
                        break;

                    default:
                        newMessageData = { text: data.data.response[0].text, alert_type: '' };
                        commit('resetLastIntent');
                        break;
                }

                // Lógica para despliegue de alerta
                if (data.alert_type !== '') {
                    newMessageData = {
                        text: '¡Parece que algo no anda bien!...',
                        alert_type: data.alert_type
                    }
                }

                commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
                checkChatNotifications({ commit, state });

            } catch (error) {
                console.log(error);
                newMessageData = {
                    text: '¡Parece que algo no anda bien!...',
                    alert_type: 'option_server_error'
                }
                commit("updateLastMessageOfArray", newMessageData);
                commit("saveLastMessage", 'chat');
                commit("updateLastRequestChat", { type: 'option', intent, text });
                checkChatNotifications({ commit, state });
            }

        },
        async startAgentDerivation({ commit, state }) {
            const params = { header: state.token };
            try {
                const { data } = await requests.startAgentDerivation(params);
                checkChatNotifications({ commit, state });
                if (data.alert_type !== '') {
                    setTimeout(() => {
                        commit("updateAgentDerivationStatus", false);
                        commit("saveMessage", { text: "", type: "bot", options: {}, alert: {} });
                        EventBus.$emit("scrollHandler");
                        EventBus.$emit("activateLoading", true);
                        if (data.alert_type === 'agent_interaction_failed_no_agent') {
                            setTimeout(() => {
                                commit("updateLastMessageOfArray", {
                                    text: `Lo sentimos. En estos momentos, <b>no tenemos agentes disponibles.</b> </br> ¿Deseas intentarlo nuevamente?`,
                                    options: { type: "confirmation_agent_derivation" },
                                });
                                EventBus.$emit("activateLoading", false);
                                commit("saveLastMessage", 'chat');
                                checkChatNotifications({ commit, state });
                                EventBus.$emit("scrollHandler");
                            }, 2000);
                        }
                        else {
                            setTimeout(() => {
                                commit("updateLastMessageOfArray", {
                                    text: `¡Lo sentimos! No hemos podido derivarte a nuestros agentes. ¿Deseas volver a intentarlo?`,
                                    options: { type: "confirmation_agent_derivation" },
                                });
                                EventBus.$emit("activateLoading", false);
                                commit("saveLastMessage", 'chat');
                                checkChatNotifications({ commit, state });
                                EventBus.$emit("scrollHandler");
                            }, 2000);
                        }
                    }, 5000);
                }
            } catch (error) {
                console.log(error);
                commit("updateAgentDerivationStatus", false);
                commit("saveMessage", { text: "", type: "bot", options: {}, alert: {} });
                EventBus.$emit("scrollHandler");
                EventBus.$emit("activateLoading", true);
                setTimeout(() => {
                    commit("updateLastMessageOfArray", {
                        text: `¡Lo sentimos! No hemos podido derivarte a nuestros agentes. ¿Deseas volver a intentarlo?`,
                        options: { type: "confirmation_agent_derivation" },
                    });
                    EventBus.$emit("activateLoading", false);
                    commit("saveLastMessage", 'chat');
                    checkChatNotifications({ commit, state });
                    EventBus.$emit("scrollHandler");
                }, 1000);
            }
        },
        async startAgentDerivationGenesys({ commit, state }) {
            const params = { header: state.token };
            try {
                const { data } = await requests.startAgentDerivation(params);
                checkChatNotifications({ commit, state });
                if (data.alert_type !== '') {
                    setTimeout(() => {
                        commit("updateAgentDerivationStatus", false);
                        commit("saveMessage", { text: "", type: "bot", options: {}, alert: {} });
                        EventBus.$emit("scrollHandler");
                        EventBus.$emit("activateLoading", true);
                        if (data.alert_type === 'agent_interaction_failed_no_agent') {
                            setTimeout(() => {
                                commit("updateLastMessageOfArray", {
                                    text: `Lo sentimos. En estos momentos, <b>no tenemos agentes disponibles.</b> </br> ¿Deseas intentarlo nuevamente?`,
                                    options: { type: "confirmation_agent_derivation" },
                                });
                                EventBus.$emit("activateLoading", false);
                                commit("saveLastMessage", 'chat');
                                checkChatNotifications({ commit, state });
                                EventBus.$emit("scrollHandler");
                            }, 2000);
                        }
                        else {
                            setTimeout(() => {
                                commit("updateLastMessageOfArray", {
                                    text: `¡Lo sentimos! No hemos podido derivarte a nuestros agentes. ¿Deseas volver a intentarlo?`,
                                    options: { type: "confirmation_agent_derivation" },
                                });
                                EventBus.$emit("activateLoading", false);
                                commit("saveLastMessage", 'chat');
                                checkChatNotifications({ commit, state });
                                EventBus.$emit("scrollHandler");
                            }, 2000);
                        }
                    }, 5000);
                }
            } catch (error) {
                console.log(error);
                commit("updateAgentDerivationStatus", false);
                commit("saveMessage", { text: "", type: "bot", options: {}, alert: {} });
                EventBus.$emit("scrollHandler");
                EventBus.$emit("activateLoading", true);
                setTimeout(() => {
                    commit("updateLastMessageOfArray", {
                        text: `¡Lo sentimos! No hemos podido derivarte a nuestros agentes. ¿Deseas volver a intentarlo?`,
                        options: { type: "confirmation_agent_derivation" },
                    });
                    EventBus.$emit("activateLoading", false);
                    commit("saveLastMessage", 'chat');
                    checkChatNotifications({ commit, state });
                    EventBus.$emit("scrollHandler");
                }, 1000);
            }
        }
    },
    getters: {
        getLastMessage(state) {
            return state.messagesChat[state.messagesChat.length - 1];
        },
        lastMessageIsTyping(state) {
            return state.messagesChat[state.messagesChat.length - 1].text === '' &&
                state.messagesChat[state.messagesChat.length - 1].type === 'agent';
        },
        getMessageOutOfTime() {
            return `¡Lo siento! En estos momentos nuestros ejecutivos <b>no se encuentran disponibles.</b>
            <br/><br/>
            Te invitamos a contactarnos de <b>Lunes a Jueves de 8:30 a 18:00 horas y Viernes de 8:30 a 16:00 horas.</b>
            <br/><br/>
            Si eres beneficiario y necesitas orientación médica, <b>llámanos al 600 6000 262 - Opción 3.</b>
            <br/><br/>
            ¿Qué otra consulta necesitas que te ayude a resolver?`;
        }
    },
})

// watch the store to update the status
setInterval(() => {
    store.commit('setTimeLastMessage');
    /* if (store.state.agentDerivationStatus) {
        store.commit('setDerivationPendingTime');
    } */
}, 1000);

function checkChatNotifications({ commit, state }) {

    if (state.activePage === 'chat') {

        if (state.statusChatbot) {
            commit("updateHasNotification", { type: 'chat', show: false })
        } else {
            EventBus.$emit('playNotificationSound');
            commit("updateHasNotification", { type: 'chat', show: true })
        }
    }

}

export default store;

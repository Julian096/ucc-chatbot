export const handleStyle = () => {
  var divs = document.querySelectorAll('div[class*="primary"]');
  var ps = document.querySelectorAll('p[class*="primary"]');
  var inputs = document.querySelectorAll('input[class*="primary"]');

  // DIVs
  divs = Array.from(divs);
  divs.map((div) => {
    let hasBorder = false;
    let isButton = false
    div.classList.forEach((element) => {
      if (element.includes("border")) {
        hasBorder = true;
      }
      if (element.includes("button")) {
        isButton = true;
      }
    });
    if (!hasBorder) {
      div.style.backgroundColor = process.env.VUE_APP_COLOR_PRIMARY;
    } else {
      div.style.borderColor = process.env.VUE_APP_COLOR_PRIMARY;
    }
    if (isButton) {
      div.addEventListener("mouseover", function () {
        div.style.opacity = 0.8;
      });
      div.addEventListener("mouseout", function () {
        div.style.opacity = 1;
      });
    }
  });

  // Ps
  ps = Array.from(ps);
  ps.map((p) => {
    p.style.color = process.env.VUE_APP_COLOR_PRIMARY;
  });

  // INPUTs
  inputs = Array.from(inputs);
  inputs.map((input) => {
    input.addEventListener("focus", function () {
      input.style.borderColor = process.env.VUE_APP_COLOR_PRIMARY;
    });
    input.addEventListener("blur", function () {
      input.style.borderColor = "#00000000";
    });
  });
}
import axios from "axios";

// axios
const baseUrlAxios = process.env.VUE_APP_URL_BACK;

// agent derivation functionality
const agentDerivationFunctionality = process.env.VUE_APP_AGENT_DERIVATION;

//Obtener token
const getToken = async (body, basic_auth) => {
    try {
        return await axios.post('/chatbot', body, basic_auth);
    } catch (error) { console.log(error) }
};

/*
Enviar mensaje a watson
@params { header = token, data = text }
*/
const sendMessage = async (params, { auth }) => {

    const config = {
        method: 'POST',
        url: '/chatbot',
        headers: {
            token: params.header,
        },
        data: { 
            method: 'message',
            message: params.data
        },
        auth
    }
    try {
        return await axios(config);
    } catch (error) { console.log(error) }
};

/*
Enviar opción seleccionada a watson
@params { header = token, data = intent }
*/
const sendOption = async (params) => {
    const config = {
        method: 'POST',
        url: 'watson/option',
        headers: { token: params.header },
        data: { intent: params.data.intent, text: params.data.text }
    }

    try {
        return await axios(config);
    } catch (error) { console.log(error) }
};

/*
Enviar mensaje a simone
@params { header = token, data = {text, agentId} }
*/
const sendMessageToSimone = async (params) => {

    const config = {
        method: 'POST',
        url: 'vue/recibirMensaje',
        headers: { token: params.header },
        data: { text: params.data.message, agentId: params.data.agentId }
    }
    try {
        return await axios(config);
    } catch (error) { console.log(error) }
};

/*
Enviar mensaje a genesys
@params { header = token, data = {text, agentId} }
*/
const sendMessageToGenesys = async (params) => {
    console.log('sendMessageToGenesys params', params);

    const config = {
        method: 'POST',
        url: 'genesys/sendMessageToGenesys',
        headers: { token: params.header },
        data: { hola: params }
    }
    try {
        return await axios(config);
    } catch (error) { console.log(error) }
};

/*
Iniciar derivación a agente
@params { header = token }
*/
const startAgentDerivation = async (params) => {
    console.log(params.header)
    const config = {
        method: 'POST',
        url: 'vue/iniciarInteraccion',
        headers: { id_inter_global: params.header, "Access-Control-Allow-Origin": "*" },
        data: { id_inter_global: params.header }
    }
    try {
        return await axios(config);
    } catch (error) { console.log(error) }
};

/*
Iniciar derivación a agente de genesys
@params { header = token }
*/
const startAgentDerivationGenesys = async (params) => {
    const config = {
        method: 'POST',
        url: 'genesys/requestAgent',
        headers: { token: params.header, "Access-Control-Allow-Origin": "*" }
    }
    try {
        return await axios(config);
    } catch (error) { console.log(error) }
};

export const pusherId = process.env.VUE_APP_PUSHER_ID;
export const pusherConfig = { cluster: process.env.VUE_APP_PUSHER_CLUSTER }
export const requests = { getToken, sendMessage, sendOption, sendMessageToSimone, sendMessageToGenesys, startAgentDerivation, startAgentDerivationGenesys };
export { baseUrlAxios };
export { agentDerivationFunctionality };
const alert_types = {
    // cuando hay un error de servidor general
    'server_error': {
        mode: 'danger',
        backgroundColor: 'red',
        title: '¡Oops!',
        description: 'Al parecer estamos experimentando problemas técnicos. Por favor inténtalo nuevamente.'
    },
    'message_server_error': {
        mode: 'danger',
        backgroundColor: 'red',
        title: '¡Oops!',
        description: 'No hemos podido enviar tu mensaje. Por favor inténtalo nuevamente.'
    },
    'option_server_error': {
        mode: 'danger',
        backgroundColor: 'red',
        title: '¡Oops!',
        description: 'No hemos podido enviar la opción seleccionada. Por favor inténtalo nuevamente.'
    },
    "agent_interaction_finished": {
        mode: 'info',
        backgroundColor: 'pacific',
        title: '¡La conversación con nuestro ejecutivo ha finalizado!',
        description: `${process.env.VUE_APP_ASSISTANT_NAME || "Assistant"} está lista para responder otras consultas.`
    },
    "agent_interaction_failed": {
        title: 'No hemos podido conectarte con nuestros agentes',
    }

}

export const getAlertData = (alertType) => {

    if (alertType === undefined || typeof alertType === 'undefined' || alert_types[alertType] === undefined) {
        return false;
    }

    return alert_types[alertType];

}

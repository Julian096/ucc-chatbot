// Lógica para generar base64 a partir de inputsFile
export const fileListToBase64 = async (querySelectorAll) => {
  const elements = document.querySelectorAll(querySelectorAll);
  let elementArray = Array.from(elements);
  let bufferPromises = [];
  let ticketImgFile = "";
  let ticketPdfFile = "";
  // Sacamos del Array inputs sin archivos
  elementArray = elementArray.filter((element) => element.value !== "");

  if (elementArray.length === 0) {
    return false;
  }

  elementArray.forEach((element) => {
    const file = element.files[0];
    const fileExt = file.name.split(".").pop();
    const fileType = element.getAttribute("data-document");
    let fileBuffer = "";

    if (fileType === "ticket") {
      if (fileExt !== "pdf") {
        ticketImgFile = file;
      } else {
        ticketPdfFile = file;
      }
    } else {
      bufferPromises.push({ ext: fileExt, file: file, type: fileType });
    }
  });

  if (ticketImgFile !== "") {
    this.ticketBase64 = await this.convertImageToPdf(
      ticketImgFile,
      "base64"
    );
  }

  if (ticketPdfFile !== "") {
    await this.convertToBase64(ticketPdfFile).then((data) => {
      this.ticketBase64 = data;
    });
  }

  if (bufferPromises.length !== 0) {
    const bufferImgPromises = bufferPromises
      .filter((element) => element.ext !== "pdf")
      .map((element) =>
        this.convertImageToPdf(element.file, "arraybuffer")
      );
    const bufferPdfPromises = bufferPromises
      .filter((element) => element.ext === "pdf")
      .map((element) => this.fileToPdfBuffer(element.file));

    const imgArrayBuffers = await Promise.all(bufferImgPromises);
    const pdfArrayBuffers = await Promise.all(bufferPdfPromises);

    const mergeArrayBuffers = [...imgArrayBuffers, ...pdfArrayBuffers];

    const mergedPdf = await this.mergePdfs(mergeArrayBuffers);
    const pdfBlob = new Blob([mergedPdf], { type: "application/pdf" });

    await this.convertToBase64(pdfBlob).then((data) => {
      this.medicalOrderBase64 = data;
    });
  }
  console.log("boleta", this.ticketBase64);
  console.log("orden", this.medicalOrderBase64);
}
// Convierte file image a base64 o arrayBuffer
const convertImageToPdf = async (file, resultType) => {
  const fileExt = file.name.split(".").pop();
  const pdf = new jsPDF();
  let imgBase64 = "";
  let imgResult = "";

  if (
    fileExt !== "png" &&
    fileExt !== "jpg" &&
    resultType === undefined &&
    typeof resultType === "undefined"
  ) {
    return false;
  }

  await this.convertToBase64(file).then((data) => {
    imgBase64 = data;
  });

  const imgFormat = fileExt.toUpperCase();
  const imgProps = pdf.getImageProperties(imgBase64);

  const pageWidth = pdf.internal.pageSize.getWidth();
  const pageHeight = pdf.internal.pageSize.getHeight();

  const widthRatio = pageWidth / imgProps.width;
  const heightRatio = pageHeight / imgProps.height;
  const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

  const imgWidth = imgProps.width * ratio;
  const imgHeight = imgProps.height * ratio;

  const marginX = (pageWidth - imgWidth) / 2;
  const marginY = (pageHeight - imgHeight) / 2;

  pdf.addImage(imgBase64, imgFormat, marginX, marginY, imgWidth, imgHeight);

  if (resultType === "base64") {
    await this.convertToBase64(pdf.output("blob")).then((data) => {
      imgResult = data;
    });
  }

  if (resultType === "arraybuffer") {
    await this.convertToArrayBuffer(pdf.output("blob")).then((data) => {
      imgResult = data;
    });
  }
  return imgResult;
}
// Convierte un archivo file or blob a base64
const convertToBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = () => resolve(fileReader.result);
    fileReader.onerror = (error) => reject(error);
  });
}
// Convierte un archivo file or blob a arrayBuffer
const convertToArrayBuffer = (file) => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);
    fileReader.onload = () => resolve(fileReader.result);
    fileReader.onerror = (error) => reject(error);
  });
}
//Merge de pdfs-arraybuffer a un solo pdf
const mergePdfs = async (pdfsToMerges) => {
  const mergedPdf = await PDFDocument.create();
  const actions = pdfsToMerges.map(async (pdfBuffer) => {
    const pdf = await PDFDocument.load(pdfBuffer);
    const copiedPages = await mergedPdf.copyPages(
      pdf,
      pdf.getPageIndices()
    );
    copiedPages.forEach((page) => {
      // console.log("page", page.getWidth(), page.getHeight());
      mergedPdf.addPage(page);
    });
  });
  await Promise.all(actions);
  const mergedPdfFile = await mergedPdf.save();
  return mergedPdfFile;
}
// Convierte file de inputFile a ArrayBuffer
const fileToPdfBuffer = async (file) => {
  let pdfBuffer = "";
  await this.convertToArrayBuffer(file).then((data) => {
    pdfBuffer = data;
  });
  return pdfBuffer;
}